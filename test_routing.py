#!/usr/bin/python3

import swarm_sim as ss
from simu import UPDATE_FREQ, MAXTEMPS

# Nodes for the tests
nb_nodes = 5
nodes = []
for i in range(nb_nodes):
    nodes.append(ss.Node(i, i * 19000, 0, 0))

nodes.append(ss.Node(nb_nodes, nb_nodes * 19000 + 50000,0,0))
swarm = ss.Swarm(0, nodes)

def test_init_routing():
    """
    Test init_routing()
    Génère 5 nodes en ligne séparés de 19 Km, on vérifie que tous les nodes soient connectés à leurs voisins les plus poche
    """
    swarm.init_routing()
    for node in swarm.nodes:
        #print(f'node {node.id}: {[node.id for node in node.dict_neighbors[20]]}, {[node.id for node in node.dict_neighbors[40]]}, {[node.id for node in node.dict_neighbors[60]]}, {[node.id for node in node.dict_neighbors[">60"]]}')
        for node_bis in node.dict_neighbors[20]:
            if node.compute_dist(node_bis) > 20000:
                return False
        for node_bis in node.dict_neighbors[40]:
            if node.compute_dist(node_bis) > 40000 or node.compute_dist(node_bis) <= 20000:
                return False
        for node_bis in node.dict_neighbors[60]:
            if node.compute_dist(node_bis) > 60000 or node.compute_dist(node_bis) <= 40000:
                return False
        for node_bis in node.dict_neighbors[">60"]:
            if node.compute_dist(node_bis) <= 60000:
                return False
    return True


def test_update_routing():
    """
    Test update_routing()
    Déplace les 5 nodes, on vérifie que tous les nodes restent connectés à leurs voisins les plus proches
    """
    for i in range(MAXTEMPS):
        for node in swarm.nodes:
            node.x = node.x * 1.5
        if i % UPDATE_FREQ == 0:
            swarm.init_routing()
        else:
            swarm.update_routing(swarm)
        for j in range(nb_nodes):
            for k in range(j+1, nb_nodes):
                #print(f'Connexion entre le node {j} et le node {k}')
                # Vérifie si le contact fonctionne que le node soit à moins de 60 Km
                if (swarm.contacter(swarm.nodes[j], swarm.nodes[k])) != (swarm.nodes[j].compute_dist(swarm.nodes[k]) <= 60000):
                    # Si le node est à plus de 60 Km et que le contact fonctionne, vérifier que toutes ses tables soient vides
                    if swarm.nodes[j].dict_neighbors[20] != [] or swarm.nodes[j].dict_neighbors[40] != [] or swarm.nodes[j].dict_neighbors[60] != []:
                        return False
            swarm.fin_iteration()
    return True


if __name__ == '__main__':
    if test_init_routing():
        print("test_init_routing : OK ✅")
    else:
        print("test_init_routing : KO ❌")
    if test_update_routing():
        print("test_update_routing : OK ✅")
    else:
        print("test_update_routing : KO ❌")
    #swarm.test()