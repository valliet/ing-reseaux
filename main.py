#!/usr/bin/python3

from panda3d.core import TextNode

from direct.gui.OnscreenText import OnscreenText
from direct.gui.DirectGui  import DirectSlider
from direct.gui.DirectGui  import DirectButton

from direct.showbase.ShowBase import ShowBase

from panda3d.core import LineSegs


import sys
from simu import simulation
# input utilisateur: demander une valeur pour le temps, le nombre de nodes à déconnecter et la fréquence de mise à jour des routes
MAXTEMPS_input = input("Entrez le temps maximal de la simulation (entier entre 1 et 10000, défaut: 100): ")
MAXTEMPS = int(MAXTEMPS_input) if MAXTEMPS_input != "" else 100

N_input = input("Entrez le nombre de nodes à déconnecter (entier entre 0 et 100, défaut: 10): ")
N = int(N_input) if N_input != "" else 10

UPDATE_FREQ_input = input("Entrez la fréquence à laquelle les tables de routage sont mises à jour (entier entre 1 et MAXTEMPS, défaut: 5): ")
UPDATE_FREQ = min(int(UPDATE_FREQ_input), MAXTEMPS) if UPDATE_FREQ_input != "" else 5

[test_results, Swarms] = simulation(MAXTEMPS, N, UPDATE_FREQ)
# test_results: [list of failed attempts per time without disconnection, list of failed attempts per time with disconnections]
SAT_COUNT = len(Swarms[0].nodes)

class MyApp(ShowBase):
    def genLabelText(self, text, i):
        return OnscreenText(text = text, pos = (-1.3, .95-.05*i), fg=(1,1,1,1),
                            align = TextNode.ALeft, scale = .05, mayChange = 1)

    def __init__(self):
        ShowBase.__init__(self)
        base.disableMouse()

        # Pseudo-constants
        self.titleString = "Projet Inginierie des Réseaux, Q.FRATY, T.KOTSONIS, A.BECKR, T.VALLIER, E.PICCO"
        self.renderRatio = 1.0e-6
        self.minCameraDistance = 1.0
        self.maxCameraDistance = 4000.0
        self.zoomPerSecond = 1.8

        # Camera parameters
        base.camLens.setFar(8.0e12)
        base.camLens.setFov(100.0)

        # Moon parameters
        self.moonSize = 7.4e10

        # Set the background to the stars texture
        base.setBackgroundColor(0, 0, 0)
        self.sky = loader.loadModel("models/solar_sky_sphere")
        self.sky_tex = loader.loadTexture("models/stars_1k_tex.jpg")
        self.sky.setTexture(self.sky_tex, 1)
        self.sky.reparentTo(render)
        self.sky.setScale(10000000)

        #This code puts the standard title and instruction text on screen
        self.titleText = OnscreenText(text=self.titleString,
                                      style=1, fg=(1,1,0,1),
                                      pos=(0.5,-0.95), scale = .04)
        
        #Metrics
        self.testResultText = self.genLabelText("", 0)
        self.testResultText['pos'] = (0.5, .9)
        
        self.testResultText2 = self.genLabelText("", 0)
        self.testResultText2['pos'] = (0.5, .95)

        # Load the moon model
        self.moon = loader.loadModel("models/planet_sphere")
        self.moon_tex = loader.loadTexture("models/moon_1k_tex.jpg")
        self.moon.setTexture(self.moon_tex, 1)
        self.moon.setScale(self.moonSize * self.renderRatio)
        self.moon.setPos(0.0, 0.0, 0.0)
        self.moon.reparentTo(render)
        self.sat = [None] * SAT_COUNT

        self.swarmCenter = [0.0, 0.0, 0.0]
        self.line_nodes_20 = []  # List to store line nodes
        self.line_nodes_40 = []  # List to store line nodes
        self.line_nodes_60 = []  # List to store line nodes
        self.line_nodes_further = []  # List to store line nodes
        self.follow = True
        self.displayedLinks20 = False
        self.displayedLinks40 = False
        self.displayedLinks60 = False
        self.displayedLinksFurther = False

        # Load all the satellites
        for i in range(SAT_COUNT):
            self.sat[i] = loader.loadModel("models/planet_sphere")
            self.sat[i].setScale(1000)
            self.sat[i].setColor(1, 1, 1, 1)
            self.sat[i].setPos(Swarms[0].nodes[i].x * self.renderRatio, Swarms[0].nodes[i].y * self.renderRatio, Swarms[0].nodes[i].z * self.renderRatio)
            self.sat[i].setTag('satIndex', str(i))
            self.sat[i].reparentTo(render)
            self.swarmCenter = self.swarmCenter + [Swarms[0].nodes[i].x, Swarms[0].nodes[i].y, Swarms[0].nodes[i].z]

        self.swarmCenter = [self.swarmCenter[0] / SAT_COUNT, self.swarmCenter[1] / SAT_COUNT, self.swarmCenter[2] / SAT_COUNT]

        # Setup events for escape : exit from app
        self.accept("escape", sys.exit)

        # Add task to move the camera
        self.taskMgr.add(self.moveCameraTask, "moveCameraTask")
        self.keyMap = {"left":0, "right":0, "up":0, "down":0, "z":0, "q":0, "s":0, "d":0, "space":0, "lshift":0}
        # Setup down events for arrow keys : rotating camera latitude and longitude
        self.accept("arrow_left", self.setKey, ["left",1])
        self.accept("arrow_right", self.setKey, ["right",1])
        self.accept("arrow_up", self.setKey, ["up",1])
        self.accept("arrow_down", self.setKey, ["down",1])
        self.accept("arrow_left-up", self.setKey, ["left",0])
        self.accept("arrow_right-up", self.setKey, ["right",0])
        self.accept("arrow_up-up", self.setKey, ["up",0])
        self.accept("arrow_down-up", self.setKey, ["down",0])
        # Setup down events for zqsd keys : moving camera
        self.accept("z", self.setKey, ["z",1])
        self.accept("q", self.setKey, ["q",1])
        self.accept("s", self.setKey, ["s",1])
        self.accept("d", self.setKey, ["d",1])
        self.accept("z-up", self.setKey, ["z",0])
        self.accept("q-up", self.setKey, ["q",0])
        self.accept("s-up", self.setKey, ["s",0])
        self.accept("d-up", self.setKey, ["d",0])
        # Setup space and ctrl events : up and down camera
        self.accept("space", self.setKey, ["space",1])
        self.accept("space-up", self.setKey, ["space",0])
        self.accept("lshift", self.setKey, ["lshift",1])
        self.accept("lshift-up", self.setKey, ["lshift",0])


        # Add a scrollbar to control the time of the simulation
        self.timeDirectSlider = DirectSlider(range=(0, MAXTEMPS-1), value=0, pageSize=1, pos=(0.0, 0.0, -0.9) ,command=self.updateSatellites)

        # Add buttons
        self.toggleFollow = DirectButton(text="Suivre", pos=(-1, 0.0, 0.5), scale=0.05, command=self.toggleFollow)
        self.play = DirectButton(text="Play", pos=(-1, 0.0, 0.4), scale=0.05, command=self.playSimulation)
        self.toggleLinksButton20 = DirectButton(text="Liens 20", pos=(-1, 0.0, 0.9), scale=0.05, command=self.toggleLinks20)
        self.toggleLinksButton40 = DirectButton(text="Liens 40", pos=(-1, 0.0, 0.8), scale=0.05, command=self.toggleLinks40)
        self.toggleLinksButton60 = DirectButton(text="Liens 60", pos=(-1, 0.0, 0.7), scale=0.05, command=self.toggleLinks60)
        self.toggleLinksButtonFurther = DirectButton(text="Liens 60+", pos=(-1, 0.0, 0.6), scale=0.05, command=self.toggleLinksFurther)

        self.isPlaying = False
        self.playButtonTask = None
        
    def playSimulation(self):
        if self.isPlaying:
            self.isPlaying = False
            self.play['text'] = "Play"
            self.taskMgr.remove("playTask")
        else:
            self.isPlaying = True
            self.play['text'] = "Stop"
            self.taskMgr.add(self.playTask, "playTask")
        
    def playTask(self, task):
        current_time = float(self.timeDirectSlider['value'])
        if current_time < MAXTEMPS:
            self.timeDirectSlider['value'] = current_time + MAXTEMPS / 100
            if int(current_time + 0.1) > int(current_time):
                self.updateSatellites()
        else:
            self.playSimulation()
            self.timeDirectSlider['value'] = MAXTEMPS
        return task.cont

    def toggleFollow(self):
        self.follow = not self.follow
        if self.follow:
            self.updateSatellites()

    def toggleLinks20(self):
        self.displayedLinks20 = not self.displayedLinks20
        self.updateSatellites()
    def toggleLinks40(self):
        self.displayedLinks40 = not self.displayedLinks40
        self.updateSatellites()
    def toggleLinks60(self):
        self.displayedLinks60 = not self.displayedLinks60
        self.updateSatellites()
    def toggleLinksFurther(self):
        self.displayedLinksFurther = not self.displayedLinksFurther
        self.updateSatellites()

    # Define a function to update all satellites positions
    def updateSatellites(self):
            
        # Remove the stored line nodes from the scene
        for line_np in self.line_nodes_20:
            line_np.removeNode()
        for line_np in self.line_nodes_40:
            line_np.removeNode()
        for line_np in self.line_nodes_60:
            line_np.removeNode()
        for line_np in self.line_nodes_further:
            line_np.removeNode()

        t = int(self.timeDirectSlider['value'])
        self.swarmCenter = [0, 0, 0]
        
        
        # Number of failed attemps at time t
        self.testResultText['text'] = "Nombre d'appels échoués : {}".format(test_results[1][t])
        # Number of disconnected nodes at time t
        self.testResultText2['text'] = "Nombre de noeuds déconnectés : {}".format(N)


        for i in range(SAT_COUNT):
            self.sat[i].setPos(
                Swarms[t].nodes[i].x, Swarms[t].nodes[i].y, Swarms[t].nodes[i].z
            )
            self.swarmCenter[0] += Swarms[t].nodes[i].x
            self.swarmCenter[1] += Swarms[t].nodes[i].y
            self.swarmCenter[2] += Swarms[t].nodes[i].z
            if not Swarms[t].nodes[i].alive:
                self.sat[i].setColor(1, 0, 0, 1)
            else:
                self.sat[i].setColor(1, 1, 1, 1)
                # Create lines between neighbors
                neighbors = Swarms[t].nodes[i].dict_neighbors
                if (self.displayedLinks20):
                    self.addLinks(t, i, neighbors[20], (0, 1, 0, 1), self.line_nodes_20)
                if (self.displayedLinks40):
                    self.addLinks(t, i, neighbors[40], (1, 1, 0, 1), self.line_nodes_40)
                if (self.displayedLinks60):
                    self.addLinks(t, i, neighbors[60], (1, 0, 0, 1), self.line_nodes_60)
                if (self.displayedLinksFurther):
                    self.addLinks(t, i, neighbors[">60"], (0, 0, 1, 1), self.line_nodes_further)
        self.swarmCenter[0] /= SAT_COUNT
        self.swarmCenter[1] /= SAT_COUNT
        self.swarmCenter[2] /= SAT_COUNT
        if self.follow:
            self.camera.setPos(
                self.swarmCenter[0] * 1.1, self.swarmCenter[1] * 1.1, self.swarmCenter[2] * 1.1
            )
            self.camera.lookAt(self.moon)

    def addLinks(self, t, i, neighbors, color, line_nodes):
        for neighbor in neighbors:
            if neighbor.alive:
                sat_pos = (
                    Swarms[t].nodes[i].x,
                    Swarms[t].nodes[i].y,
                    Swarms[t].nodes[i].z,
                )
                neighbor_pos = (neighbor.x, neighbor.y, neighbor.z)
                lines = LineSegs()
                lines.setThickness(1)
                lines.setColor(color)
                lines.moveTo(sat_pos[0], sat_pos[1], sat_pos[2])
                lines.drawTo(neighbor_pos[0], neighbor_pos[1], neighbor_pos[2])
                line_np = render.attachNewNode(lines.create())
                line_nodes.append(line_np)  # Store line node in the list

    def moveCameraTask(self, task):
        # Move the camera according to the key pressed
        if (self.keyMap["left"]!=0):
            self.camera.setH(self.camera.getH() + 0.5)
            self.follow = False
        if (self.keyMap["right"]!=0):
            self.camera.setH(self.camera.getH() - 0.5)
            self.follow = False
        if (self.keyMap["up"]!=0):
            self.camera.setP(self.camera.getP() - 0.5)
            self.follow = False
        if (self.keyMap["down"]!=0):
            self.camera.setP(self.camera.getP() + 0.5)
            self.follow = False
        # Add zqsd keys to move the camera
        if (self.keyMap["z"]!=0):
            self.camera.setY(self.camera, -20000 * globalClock.getDt())
            self.follow = False
        if (self.keyMap["s"]!=0):
            self.camera.setY(self.camera, +20000 * globalClock.getDt())
            self.follow = False
        if (self.keyMap["q"]!=0):
            self.camera.setX(self.camera, -20000 * globalClock.getDt())
            self.follow = False
        if (self.keyMap["d"]!=0):
            self.camera.setX(self.camera, +20000 * globalClock.getDt())
            self.follow = False
        # Add space and shift keys to move the camera up and down
        if (self.keyMap["space"]!=0):
            self.camera.setZ(self.camera, +20000 * globalClock.getDt())
            self.follow = False
        if (self.keyMap["lshift"]!=0):
            self.camera.setZ(self.camera, -20000 * globalClock.getDt())
            self.follow = False
        return task.cont
    
    def setKey(self, key, value):
        self.keyMap[key] = value
app = MyApp()
app.run()