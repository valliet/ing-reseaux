import numpy as np
import pandas as pd
from tqdm import tqdm
from dataclasses import dataclass
from swarm_sim import *
import networkx as nx
import random

# Chemin vers le fichier source des donnes
PATH = './Traces.csv'
# Nombre de temps a analyser
# Constantes de distance
MAX_RANGE = 60000
MID_RANGE = 40000
MIN_RANGE = 20000
# Routing update frequency: f = x means every x units of time
###
### Manipulation du fichier source.
###
def simulation(MAXTEMPS, N, UPDATE_FREQ):
  print("### Importation des données ### ")
  df = pd.read_csv(PATH, header=0)

  print("### Reformatage des données : ajout d'un index sur le temps ### ")
  #On genere un tableau qui servira d'index de temps pour chaque echantillon
  names = [str(i) for i in range(1, 10001)]
  #On modifie le df pour ajouter en tete de colone notre index de temps
  save = df.columns
  df.columns = names
  df.loc[-1] = save
  df.index = df.index + 1 
  df = df.sort_index()
  print("### Reformatage des données : ajout d'un index sur les satelites ### ")
  # On genere les noms des satelite 
  satnames = [['satx'+ str(i), 'saty'+ str(i), 'satz'+ str(i)] for i in range(1, SAT_COUNT + 1)]
  satnames = [item for sub_list in satnames for item in sub_list] # flatten
  # On applique notre index
  df['coords'] = satnames
  df = df.set_index('coords', drop=True)

  # On transpose notre df pour avoir le temps en ligne et les coordonees de satelites en colonnes
  dft = df.transpose()

  ###
  ### Mise en forme de la donnee en memoire (Creation des structures de donnees necessaires)
  ###
  # On itère sur les elements de notre df pour en extraire des Node comme implantes dans swarm_sim
  def GetNodes(time):
    return {id-1 : Node(id - 1, dft.loc[str(time)]['satx'+str(id)],
                                dft.loc[str(time)]['saty'+str(id)],
                                dft.loc[str(time)]['satz'+str(id)]) for id in range(1, 101)} 

  def GetPositions():
    return {t: GetNodes(t + 1) for t in range(MAXTEMPS)}

  def InitSwarms(Positions):
    return {t: Swarm(MAX_RANGE, list(Positions[t].values())) for t in range(MAXTEMPS)}

  # Cette fonction permet, a partir du swarm, d'extraire une matrice d'adjaceance qui prend en compte le coût des differents liens.
  def GetWeightedMatrix(Swarm):
    # On prend comme base le reseau avec une portee minimale puis on vient 
    # ajouter les chemins necessitant plus de portee en multipliant par un coefficient.
    matrix20 = Swarm.neighbor_matrix(MIN_RANGE)
    matrix40 = Swarm.neighbor_matrix(MID_RANGE)
    matrix60 = Swarm.neighbor_matrix(MAX_RANGE)
    x=0
    y=0 
    while x < len(matrix20) :
      y=0
      while y < len(matrix20):
        if (matrix20[x][y] == 0 and matrix40[x][y] == 1) :
          matrix20[x][y]=2
        if (matrix20[x][y] == 0 and matrix40[x][y] == 0 and matrix60[x][y] == 1):
          matrix20[x][y]=3 
        y=y+1
      x=x+1
    return matrix20

  def GenerateWeightedMatrix(Swarms):
    return {t: GetWeightedMatrix(Swarms[t]) for t in range(MAXTEMPS)}

  print("### Génération des tableaux de nodes en fonction du temps ### ") 
  Positions = GetPositions()

  print("### Génération des swarms correspondants a chaque instants ###")
  Swarms = InitSwarms(Positions)

  print("-------------------------------------")
  print("Simu.py run as module, only loading Swarms")
  print("-------------------------------------")
  # Swarms is a list of Swarm, each for every time unit
  # Initialize for t = 0
  Swarms[0].init_routing()
  # Update times every UPDATE_FREQ
  for i in range(1, MAXTEMPS):
    if i % UPDATE_FREQ == 0:
      Swarms[i].init_routing()
    else:
      Swarms[i].update_routing(Swarms[i-1])
  """
  Tests that send different packets in the network, count the number of failed transmissions.
  This value represents how well-connected is the network, and also enables to compare the
  networks's behaviour when som e nodes are disconnected.
  """
  #nodes_ids = random.sample(range(0, 100), N)
  # choose targeted nodes ids
  #nodes_ids = range(0, N)
# random
  nodes_ids = random.sample(range(0, 100), N)   
  test_results = [[], []] # [0] = normal, [1] = disconnected
  for i in range(0, MAXTEMPS):
    result = Swarms[i].test()
    test_results[0].append(result)
    #print("Number of failed attempts at time t = " + str(i) + " : " + str(result))
    if (N != 0):
      """
      Test again the network after having disconnected the chosen nodes
      """
      for id in nodes_ids:
        Swarms[i].nodes[id].alive = False
      result = Swarms[i].test()
      test_results[1].append(result)
      #print("Number of failed attempts at time t = " + str(i) + " after disconnecting " + str(N) + " nodes : " + str(result))
  return [test_results, Swarms]



