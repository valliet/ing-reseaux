Ceci est le readme du projet d'ingénierie des réseaux. 
L'objectif de ce projet est d'étudier la résilience d'un essaim de nano-satellites.

Critères de performances : 

Résistance aux pannes
- prévisibles
- non prévisibles

Métriques importantes 
- Rapidité de la détéction d'une panne (s)
- Fiabilité (messages reçus/envoyés)
- Rapidité du reroutage (s)
- Seuil à ne pas dépasser pour la non distribution (nb de satellites en panne)

2 Hypothèses 
- message vital
- message non vital (voix...)

Objet swarm_sim
On change le tableau neighbors en dictionnaire avec les 3 zones. (Changer update voir plus)
Ajouter une table sous forme de dictionnaire avec tous les id de nodes et le chemin (si vide -> pas de route)

Routage:
- Initialisation: Priorités aux chemins les plus courts
  - Tout le monde fait sa zone 0
  - Tout le monde fait sa zone 1
  - Check si ceux de la zone 0 n'ont pas ceux de la zone 1 en zone 0 (priorité)
  - Tout le monde fait sa zone 2
  - Check si ceux de la zone 1 n'ont pas ceux de la zone 2 en zone 0 (priorité)


  Regarder si la consommation est linéaire ou non (entre 20 40 et 60 km)



## QoS : 

  Structure du réseau : adhoc hybrid
  Découverte de réseau (rayon de 60km) : découverte de l'intrazone
    -> hello it's me : . degré
    -> ajout à la table (distance, id, degré) donc lease

  Quelles sont les métriques que l'on retient? 
  Résilience du réseau : mesurer grâce au taux de paquet perdu lors d'une panne
  Réception des infos critiques (les infos des bords sur le graphe)
  

    def contacter(node1, node2):
        if node2 dans voisins de node1:
            return 1
        else:
            return somme de node1.voisins60.contacter(node2)



## Test des fichiers 